#!/usr/bin/perl

BEGIN {
	delete $ENV{http_proxy}
}

use strict;
use warnings;
use XML::Simple;
use LWP::Simple qw($ua get);
use CGI;
use Data::Dumper;

# Send headers asap.
print "Content-type: text/html\n\n";

# Reduce timeout to 5s.
$ua->timeout(5);

my $cgi = new CGI();
my %vars = $cgi->Vars();
my $url = $cgi->url();
my $jenkins_url = $ENV{JENKINS_URL};
my $name = 'Yep Yep Jenkins UI powered by common sense';

sub edie {
	# Something something to persent error to the user.
	# simple die() won't do.
	print @_;
	exit(1);
}

edie "Missing JENKINS_URL env variable" unless defined $jenkins_url;

sub query_api {
	my $what = 'main';
	my $raw_xml;
	my $xml;
	my $view;
	if (scalar @_ >= 1) {
		$what = shift;
	}

	if ($what eq 'view') {
		$view = shift;
	}

	if ($what eq 'main') {
		$raw_xml = get("$jenkins_url/api/xml") or edie "Failed to fetch /api/xml: $!.";
	} elsif ($what eq 'queue') {
		$raw_xml = get("$jenkins_url/queue/api/xml") or edie "Failed to fetch /queue/api/xml: $!.";
	} elsif ($what eq 'nodes') {
		$raw_xml = get("$jenkins_url/computer/api/xml") or edie "Failed to fetch /computer/api/xml: $!.";
	} elsif ($what eq 'view') {
		$raw_xml = get("$jenkins_url/view/$view/api/xml") or edie "Failed to fetch /view/$view/api/xml: $!.";
	}
	$xml = XMLin($raw_xml, KeyAttr => [], ForceArray => ['item']) if defined $raw_xml or edie "Empty xml response :<";
	return $xml;
}

sub print_job_link {
	my $job_name = shift;
	my $status_color = shift;
	my $reason = shift;

	my $anchor_style = '';
	my $prefix = '';
	my $sufix = '';
	if ($status_color =~ /^(red|yellow)/) {
		$prefix = "<span style='color:#F00; font-weight:bold;'>[RED]</span> ";
	} elsif ($status_color =~ /^aborted/) {
		$prefix = "<span style='color:#F00; font-weight:bold;'>[ABORTED]</span> ";
	} elsif ($status_color =~ /^blue/) {
		$prefix = "<span style='color:#66CCCC; font-weight:bold;'>[BLUE]</span> ";
	} elsif ($status_color eq 'grey') {
		$prefix = "<span style='color:#777; font-weight:bold;'>[NEW]</span> ";
	} elsif ($status_color eq 'disabled') {
		$anchor_style = 'color: #777;';
		$prefix = '[DISABLED] ';
	}

	if ($status_color =~ /_anime$/) {
		$prefix = "$prefix <span style='color: #22dd22; font-weight:bold;'>[BUILDING]</span> ";
	}

	if (defined $reason) {
		
		$prefix = "$prefix  <span style='color: #FF0; font-weight:bold;'>[QUEUED]</span> ";
		$sufix = " <span style='color: #777;'>($reason)</span>" unless ($reason eq 1);
	}

	printf("<a href='%s' style='$anchor_style'>$prefix%s$sufix</a>", "$jenkins_url/job/$job_name", $job_name);
}

sub print_node_link {
	my $node_name = shift;
	my $offline = shift;
	my $temporarilyOffline = shift;
	my $offlineCauseReason = shift;
	my $prefix = '';
	my $sufix = '';
	my $anchor_style = '';
	my $status;

	if (($offline eq 'true') or ($temporarilyOffline eq 'true')) {
		$status = 'offline';
		# If the offlineCauseReason is hash, its empty.
		if (defined $offlineCauseReason and not (ref($offlineCauseReason) eq "HASH")) {
			$sufix = " <span style='color: #777;'>($offlineCauseReason)</span>";
		}
	} else {
		$status = 'online';
	}

	if ($status eq 'offline') {
		$prefix = "<span style='color:#F00; font-weight:bold;'>[OFFLINE]</span> ";
	} elsif ($status eq 'online') {
		$prefix = "<span style='color:#66CCCC; font-weight:bold;'>[ONLINE]</span> ";	
	}
	printf("<a href='%s' style='$anchor_style'>$prefix%s$sufix</a>", "$jenkins_url/computer/$node_name", $node_name);
}

print <<END;
<html>
<head>
	<title>$name</title>
	<style type="text/css">
		body {
			color: #DDDDDD;
			background-color: #202020;
			font-family: verdana;
			padding: 10px 50px 10px 50px;
			font-size: 10pt;
		}
		a {
			display: block;
			text-decoration: none;
			color: #DDD;
			padding: 5px 15px 5px 15px;
		}
		a:hover {
			background-color: #333;
		}

		input {
			border: 1px solid #555;
			background-color: #202020;
			color: #DDD;
			
		}
		form {
			color: #777;
			padding-left: 15px;
			padding-right: 15px;
			display:inline-block;
		}
	</style>
	</head>
<h2>$name</h2>
END

printf("<a style=\"display: inline-block;\" href=\"%s\">%s</a>\n", "$url", "INDEX (list all failures)");
printf("<a style=\"display: inline-block;\" href=\"%s\">%s</a>\n", "$url?page=in_progress_and_build_queue", "In Progress and Build Queue");
printf("<a style=\"display: inline-block;\" href=\"%s\">%s</a>\n", "$url?page=nodes", "List nodes");

my $search_string = (defined $vars{search} ? $vars{search} : '');

print "
<form>
	 Regex search <input type='text' name='search' value='$search_string' size='31' />
</form>
";
printf("<a style=\"display: inline-block;\" href=\"%s\">%s</a>\n", "$jenkins_url/manage", "Manage Jenkins");


printf("<h2>%s</h2>\n", "Views");

my $xml = query_api;

for (@{$xml->{view}}) {
	# First empty tab is wrongly checked as hash.
	next if (ref($_->{name}) eq "HASH");
	my $view_name_url = $_->{name};
	$view_name_url =~ s/\+/%2B/;
	printf("<a style=\"display: inline-block;\" href=\"%s\">%s</a>\n", "$url?view=$view_name_url", $_->{name});
}

if (defined $vars{view}) {
	my $view_name = "$vars{view}";

	my $xml = query_api('view', $view_name);
	printf("<h2>%s</h2>\n", "$vars{view}");
	for (@{$xml->{job}}) {
		my $job_name = $_->{name};
		my $job_color = $_->{color};
		print_job_link $job_name, $job_color;
	}

} elsif ((defined $vars{page}) && ($vars{page} eq 'in_progress_and_build_queue')) {
	my $queue_xml = query_api('queue');

	my $jobs_in_queue = 0;
	for (@{$queue_xml->{item}}) {
		$jobs_in_queue++;
	}

	my $jobs_in_progress = 0;
	for (@{$xml->{job}}) {
		$jobs_in_progress++ if ($_->{color} =~ /_anime$/);
	}

	printf("<h2>%s</h2>\n", "In Progress ($jobs_in_progress) and Build Queue ($jobs_in_queue)");

	for (@{$queue_xml->{item}}) {
		my $job_name = $_->{task}->{name};
		my $job_color = $_->{task}->{color};
		my $why = $_->{why};
		$why = 1 if (not $why);
		print_job_link $job_name, $job_color, $why;
	}

	for (@{$xml->{job}}) {
		my $job_name = $_->{name};
		my $job_color = $_->{color};
	
		if ($job_color =~ /_anime$/) {
			print_job_link $job_name, $job_color;
		}
	}

} elsif ((defined $vars{page}) && ($vars{page} eq 'nodes')) {
	printf("<h2>%s</h2>\n", "Nodes");
	my $nodes_xml = query_api('nodes');
	#print "<pre>", Dumper $nodes_xml, "</pre>";
	for (@{$nodes_xml->{computer}}) {
		# Skip master, its kinda default and /computer/master/ does not exist;
		next if $_->{displayName} eq 'master';
		print_node_link $_->{displayName}, $_->{offline}, $_->{temporarilyOffline}, $_->{offlineCauseReason};
	}
} elsif ((defined $vars{search})) {
	my $search = $vars{search};
	chomp($search);
	my $regex_search = qr($search);

	printf("<h2>%s</h2>\n", "Search results for '$search'");

	for (@{$xml->{job}}) {
		my $job_name = $_->{name};
		my $job_color = $_->{color};

		if ($job_name =~ m/$regex_search/) {
			print_job_link $job_name, $job_color;
		}
	}

} else {
	# Le default page.
	printf("<h2>%s</h2>\n", "Failures");
	# Aborted
	for (@{$xml->{job}}) {
		my $job_name = $_->{name};
		my $job_color = $_->{color};
		if ($job_color =~ /^aborted/) {
			print_job_link $job_name, $job_color;
		}
	}
	
	for (@{$xml->{job}}) {
		my $job_name = $_->{name};
		my $job_color = $_->{color};
		# Print all not 'okey' jobs.
		if ((!($job_color =~ m/blue/)) && (!($job_color eq 'disabled')) && (!($job_color eq 'grey')) && (!($job_color =~ /^aborted/))) {
			print_job_link $job_name, $job_color;
		}
	}
}

print "<br /><br /></body></html>\n";

